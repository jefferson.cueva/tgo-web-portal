const mongoose = require("mongoose");
const { Schema } = mongoose;

const UsersSchema = new Schema({
  first_name: String,
  last_name: String,
  Email: String,
  password: String,
  postion: String,
  group_ids: [String],
  role_ids: [String],
  avatar: String
});

mongoose.model("Users", UsersSchema);

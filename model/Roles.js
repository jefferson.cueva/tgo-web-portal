const mongoose = require("mongoose");
const { Schema } = mongoose;

const RolesSchema = new Schema({
  name: String
});
mongoose.model("Roles", RolesSchema);

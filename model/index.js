const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/TGOPortal", {
  useNewUrlParser: true
});

require("./Comments.js");
require("./Groups.js");
require("./Items.js");
require("./Nominations.js");
require("./NominationValues.js");
require("./Orders.js");
require("./Posts.js");
require("./Roles.js");
require("./Transactions.js");
require("./Users.js");

const mongoose = require("mongoose");
const { Schema } = mongoose;

const OrdersSchema = new Schema({
  user_id: String,
  item_id: String,
  quantity: String,
  created_at: String
});
mongoose.model("Orders", OrdersSchema);

const mongoose = require("mongoose");
const { Schema } = mongoose;

const TransactionsSchema = new Schema({
  user_id: String,
  type: String
});

mongoose.model("Transactions", TransactionsSchema);

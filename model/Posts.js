const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostsSchema = new Schema({
  user_id: String,
  title: String,
  content: String,
  likes: [String]
});
mongoose.model("Posts", PostsSchema);

const mongoose = require("mongoose");
const { Schema } = mongoose;

const NominationValuesSchema = new Schema({
  name: String,
  description: String,
  value: String
});
mongoose.model("NominationValues", NominationValuesSchema);

const mongoose = require("mongoose");
const { Schema } = mongoose;

const ItemsSchema = new Schema({
  name: String, 
  price: String
});

mongoose.model("Items", ItemsSchema);

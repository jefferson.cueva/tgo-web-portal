const mongoose = require("mongoose");
const { Schema } = mongoose;

const CommentsSchema = new Schema({
  user_id: String,
  value: String,
  likes: [String]
});

mongoose.model("Comments", CommentsSchema);

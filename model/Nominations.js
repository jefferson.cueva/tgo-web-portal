const mongoose = require("mongoose");
const { Schema } = mongoose;

const NominationsSchema = new Schema({
  user_id: String,
  nominee_id: String,
  admin_id: String,
  type_id: String,
  status: String
});
mongoose.model("Nominations", NominationsSchema);

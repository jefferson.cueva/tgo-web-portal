const mongoose = require("mongoose");
const { Schema } = mongoose;

const GroupsSchema = new Schema({
  user_id: String,
  name: String,
  description: String
});

mongoose.model("Groups", GroupsSchema);

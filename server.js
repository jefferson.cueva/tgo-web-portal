const express = require("express");
const app = express();
const PORT = process.env.PORT || 5000;

require("./model");

app.listen(PORT, () => {
  console.log(`connected to port ${PORT}`);
});
